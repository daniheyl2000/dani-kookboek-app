import { decimalDigest } from '@angular/compiler/src/i18n/digest';
import { Injectable } from '@angular/core';
import { recipe } from 'src/app/recipes/recipe';
import { AuthorServers } from '../author/mock-authors';
import { author } from '../author/author';
let today = new Date().toLocaleDateString()
@Injectable({
	providedIn: 'root'
})

export class RecipesServers {
    //description
    //cookingtime
    //vegan
    //serving
    //published
    
    recipes: recipe[] = [
        {   
            name: "pizza",
            image: "https://media.istockphoto.com/photos/tasty-pepperoni-pizza-and-cooking-ingredients-tomatoes-basil-on-black-picture-id1083487948?k=20&m=1083487948&s=612x612&w=0&h=ROZ5t1K4Kjt5FQteVxTyzv_iqFcX8aqpl7YuA1Slm7w=",
            description: "nice pizza" ,
            cookingTime: 30,
            vegan: true,
            serving: 1,
            published: new Date(2000, 8, 24) ,          
        },
        {   
            name: "pizza",
            image: "https://media.istockphoto.com/photos/tasty-pepperoni-pizza-and-cooking-ingredients-tomatoes-basil-on-black-picture-id1083487948?k=20&m=1083487948&s=612x612&w=0&h=ROZ5t1K4Kjt5FQteVxTyzv_iqFcX8aqpl7YuA1Slm7w=",
            description: "nice pizza" ,
            cookingTime: 30,
            vegan: false,
            serving: 1,
            published: new Date(2000, 8, 24),
        },
        {   
            name: "pizza",
            image: "https://media.istockphoto.com/photos/tasty-pepperoni-pizza-and-cooking-ingredients-tomatoes-basil-on-black-picture-id1083487948?k=20&m=1083487948&s=612x612&w=0&h=ROZ5t1K4Kjt5FQteVxTyzv_iqFcX8aqpl7YuA1Slm7w=",
            description: "nice pizza" ,
            cookingTime: 30,
            vegan: false,
            serving: 1,
            published: new Date(2000, 8, 24),
        },
        {   
            name: "pizza",
            image: "https://media.istockphoto.com/photos/tasty-pepperoni-pizza-and-cooking-ingredients-tomatoes-basil-on-black-picture-id1083487948?k=20&m=1083487948&s=612x612&w=0&h=ROZ5t1K4Kjt5FQteVxTyzv_iqFcX8aqpl7YuA1Slm7w=",
            description: "nice pizza" ,
            cookingTime: 30,
            vegan: false,
            serving: 1,
            published: new Date(2000, 8, 24),
        },
        {   
            name: "pizza",
            image: "https://media.istockphoto.com/photos/tasty-pepperoni-pizza-and-cooking-ingredients-tomatoes-basil-on-black-picture-id1083487948?k=20&m=1083487948&s=612x612&w=0&h=ROZ5t1K4Kjt5FQteVxTyzv_iqFcX8aqpl7YuA1Slm7w=",
            description: "nice pizza" ,
            cookingTime: 30,
            vegan: false,
            serving: 1,
            published: new Date(2000, 8, 24),
        },
        {   
            name: "pizza",
            image: "https://media.istockphoto.com/photos/tasty-pepperoni-pizza-and-cooking-ingredients-tomatoes-basil-on-black-picture-id1083487948?k=20&m=1083487948&s=612x612&w=0&h=ROZ5t1K4Kjt5FQteVxTyzv_iqFcX8aqpl7YuA1Slm7w=",
            description: "nice pizza" ,
            cookingTime: 30,
            vegan: false,
            serving: 1,
            published: new Date(2000, 8, 24),
        },
    ]
    constructor() {
		console.log("constructor van RecipesServers is aangeroepen");
	}
    getRecipes(): recipe[] {
		return this.recipes;
	}
}