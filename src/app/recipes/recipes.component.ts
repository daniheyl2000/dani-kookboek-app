import { Component, OnInit } from '@angular/core';
import { RecipesServers } from './mock-recipes';
import { recipe } from './recipe';
import { author } from '../author/author';
import { AuthorServers } from '../author/mock-authors';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {

  public recipes: recipe[];

  constructor(private recipesService:RecipesServers, private authorserver:AuthorServers) {
    this.recipes = recipesService.getRecipes();
  }

  ngOnInit(): void {
  }

}
