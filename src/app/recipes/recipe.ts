import { DeclarationListEmitMode } from "@angular/compiler";
import * as internal from "stream";
import { isVariableDeclaration } from "typescript";
import { author } from "../author/author";

export class recipe {

	public name: string;
    public image: string;
    public description: string;
    public cookingTime: number;
    public vegan: Boolean;
    public serving: number;
    public published: Date;

	constructor(name:string,id:number, image:string, cookingtime:number, description:string,vegan:Boolean, serving:number ,published:Date) {
		this.name = name;
        this.image = image;
        this.cookingTime = cookingtime;
        this.description = description;
        this.vegan = vegan;
        this.serving = serving;
        this.published = published;
	}
}


