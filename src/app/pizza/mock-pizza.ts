import { decimalDigest } from '@angular/compiler/src/i18n/digest';
import { Injectable } from '@angular/core';
import { pizza } from 'src/app/pizza/pizza';
import { AuthorServers } from '../author/mock-authors';
import { author } from '../author/author';
let today = new Date().toLocaleDateString()
@Injectable({
	providedIn: 'root'
})

export class PizzaServers {
    //description
    //cookingtime
    //vegan
    //serving
    //published
    
    pizzas: pizza[] = [
        {   
            id: 1,
            name: "pizza met extra pizza",
            image: "https://media.istockphoto.com/photos/tasty-pepperoni-pizza-and-cooking-ingredients-tomatoes-basil-on-black-picture-id1083487948?k=20&m=1083487948&s=612x612&w=0&h=ROZ5t1K4Kjt5FQteVxTyzv_iqFcX8aqpl7YuA1Slm7w=",
            description: "nice pizza" ,
            cookingTime: 30,
            vegan: true,
            sauce: "Tomato",
            toppings: "Mozzerala, pepperoni, Pizza",
            published: new Date(2000, 8, 24) ,          
        },
        {   
            id: 2,
            name: "pizza",
            image: "https://media.istockphoto.com/photos/tasty-pepperoni-pizza-and-cooking-ingredients-tomatoes-basil-on-black-picture-id1083487948?k=20&m=1083487948&s=612x612&w=0&h=ROZ5t1K4Kjt5FQteVxTyzv_iqFcX8aqpl7YuA1Slm7w=",
            description: "nice pizza" ,
            cookingTime: 30,
            vegan: false,
            sauce: "Tomato",
            toppings: "Mozzerala, pepperoni, Pizza",
            published: new Date(2000, 8, 24),
        },
        
    ]
    constructor() {
		console.log("constructor van RecipesServers is aangeroepen");
	}
    getPizzas(): pizza[] {
		return this.pizzas;
	}

    getPizzaById(id: number): pizza {
		return this.pizzas.filter(pizza => pizza.id == id)[0];
	}

    createPizza(param: pizza): void {
		param.id = this.pizzas.length;
		this.pizzas.push(param);
	}
	deletePizza(id: number): void {
		console.log(this.pizzas.filter(pizza => pizza.id == id));
		var index: number = this.pizzas.indexOf(this.pizzas.filter(pizza => pizza.id == id)[0]);
		this.pizzas.splice(index, 1);
	}
	updatePizza(param: pizza, id: number): void {
		if(param.name != null){
			this.pizzas.filter(pizza => pizza.id == id)[0].name = param.name;
		}
	}
}