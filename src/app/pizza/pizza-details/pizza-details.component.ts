import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { pizza } from '../pizza';
import { PizzaServers } from '../mock-pizza';

@Component({
  selector: 'app-pizza-details',
  templateUrl: './pizza-details.component.html',
  styleUrls: ['./pizza-details.component.css']
})
export class PizzaDetailsComponent implements OnInit {

  pizzaId: string|null = null;

	currentPizza:pizza | undefined;

	constructor(private route: ActivatedRoute, private pizzaEditService:PizzaServers) {
    this.pizzaId = this.route.snapshot.paramMap.get('id');
		this.currentPizza = this.pizzaEditService.getPizzaById(parseInt(this.pizzaId!));
	}

  ngOnInit(): void {
  }

  

}
