import { DeclarationListEmitMode } from "@angular/compiler";
import * as internal from "stream";
import { isVariableDeclaration, StringLiteralLike } from "typescript";
import { author } from "../author/author";

export class pizza {

    public id: number;
	public name: string;
    public image: string;
    public description: string;
    public sauce: string;
    public toppings: string;
    public cookingTime: number;
    public vegan: Boolean;
    public published: Date;

	constructor(name:string,id:number, image:string, cookingtime:number, description:string,vegan:Boolean ,published:Date, sauce: string, toppings: string) {
		this.name = name;
        this.image = image;
        this.description = description;
        this.sauce = sauce;
        this.toppings = toppings
        this.cookingTime = cookingtime;
        this.vegan = vegan;
        this.published = published;
        this.id = id;
	}
}


