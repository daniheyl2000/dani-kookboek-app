import { Component, OnInit } from '@angular/core';
import { pizza } from '../pizza';
import { PizzaServers } from '../mock-pizza';

@Component({
  selector: 'app-pizza-list',
  templateUrl: './pizza-list.component.html',
  styleUrls: ['./pizza-list.component.css']
})
export class PizzaListComponent implements OnInit {

  public pizzas: pizza[]

  constructor(private pizzaService:PizzaServers) {
    this.pizzas = pizzaService.getPizzas();
  }

  ngOnInit(): void {
  }

  delete(id:number) : void{
		this.pizzaService.deletePizza(id);
	}

}
