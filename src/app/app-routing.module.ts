import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { LayoutComponent } from './core/layout/layout.component'
import { UsecasesComponent } from './pages/about/usecases/usecases.component'
import { UsersComponent } from './users/users.component'
import { RecipesComponent } from './recipes/recipes.component'
import { PizzaListComponent } from './pizza/pizza-list/pizza-list.component'
import { PizzaDetailsComponent } from './pizza/pizza-details/pizza-details.component'
import { PizzaEditComponent } from './pizza/pizza-edit/pizza-edit.component'

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'about', component: UsecasesComponent },
      { path: 'users', component: UsersComponent },
      { path: 'recipes', component: RecipesComponent },
      { path: 'pizza', component: PizzaListComponent },
    { path: 'pizza/:id', component: PizzaDetailsComponent},
    { path: 'pizza/:id/edit', component:PizzaEditComponent}
    ]
  },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
