import { decimalDigest } from '@angular/compiler/src/i18n/digest';
import { Injectable } from '@angular/core';
import { author } from './author';

@Injectable({
	providedIn: 'root'
})
export class AuthorServers {
    public authors: author[] = [
        {
            name: "Gordon remsie",
            image: "https://i.okokorecepten.nl/recipegroups/themas/chefs/gordon-ramsay-750.jpg",
            age: 33,
            country: "England",
            id: 1

        },
        {
            name: "Gordon remsie2",
            image: "https://i.okokorecepten.nl/recipegroups/themas/chefs/gordon-ramsay-750.jpg",
            age: 33,
            country: "England",
            id: 2
        },

    ]

    public getUserById(id: number): author {
		return this.authors.filter(author => author.id == id)[0];
	}

    getAuthors(): author[] {
		return this.authors;
	}
}