import { isVariableDeclaration } from "typescript";

export class author {

	public name: string;
    public age: number;
    public country: string;
    public image: string;
    public id: number;

	constructor(name:string,id:number, image:string, age:number,country:string) {
		this.name = name;
        this.image = image;
        this.age = age;
        this.country = country;
        this.id = id;
	}
}

